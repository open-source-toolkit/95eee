# Hadoop 3.3.6 Winutils 资源文件

本仓库提供适用于 Hadoop 3.3.6 的 `winutils.exe` 和 `hadoop.dll` 文件。这些文件是运行 Hadoop 在 Windows 操作系统上所必需的。

## 文件说明

- **winutils.exe**: Hadoop 在 Windows 系统上的实用工具，用于执行一些系统级别的操作。
- **hadoop.dll**: Hadoop 在 Windows 系统上的动态链接库文件，提供必要的系统支持。

## 使用方法

1. 下载本仓库中的 `winutils.exe` 和 `hadoop.dll` 文件。
2. 将这两个文件放置在 Hadoop 安装目录的 `bin` 文件夹中。
3. 配置环境变量，确保 Hadoop 能够正确调用这些文件。

## 注意事项

- 请确保你使用的是 Hadoop 3.3.6 版本，其他版本可能需要不同的 `winutils.exe` 和 `hadoop.dll` 文件。
- 如果你在运行 Hadoop 时遇到任何问题，请检查是否正确配置了这些文件。

## 贡献

欢迎提交问题和改进建议。如果你有更好的解决方案或发现任何问题，请提交 Issue 或 Pull Request。

## 许可证

本仓库中的文件遵循 Apache License 2.0 开源协议。详细信息请参阅 [LICENSE](LICENSE) 文件。